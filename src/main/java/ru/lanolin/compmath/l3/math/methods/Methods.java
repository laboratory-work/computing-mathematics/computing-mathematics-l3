package ru.lanolin.compmath.l3.math.methods;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Methods {
	METHOD_HALF_DIVISION("Method Half Division", new MethodHalfDivision()),
	METHOD_SECANT("Method Secant", new MethodSecant()),
	METHOD_SIMPLE_ITERATION("Method Simple Iteration", new MethodSimpleIteration());

	private String description;
	@Getter private Method method;

	@Override
	public String toString() {
		return description;
	}
}
