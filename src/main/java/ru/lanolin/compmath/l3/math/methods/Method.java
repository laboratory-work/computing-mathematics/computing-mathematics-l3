package ru.lanolin.compmath.l3.math.methods;

import ru.lanolin.compmath.l3.fx.controller.MainController;
import ru.lanolin.compmath.l3.math.AnswerInformation;

import java.math.BigDecimal;

public interface Method {
	AnswerInformation solve(BigDecimal a, BigDecimal b, BigDecimal eps);

	default void setController(MainController controller){}
}
