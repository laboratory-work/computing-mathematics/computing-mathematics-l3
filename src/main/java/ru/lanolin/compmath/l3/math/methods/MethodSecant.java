package ru.lanolin.compmath.l3.math.methods;

import lombok.NonNull;
import ru.lanolin.compmath.l3.fx.I18N;
import ru.lanolin.compmath.l3.fx.controller.MainController;
import ru.lanolin.compmath.l3.fx.controller.TableData;
import ru.lanolin.compmath.l3.math.AnswerInformation;
import ru.lanolin.compmath.l3.math.Functions;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

public class MethodSecant implements Method {

	private static Functions f = Functions.getFunction();

	private MainController controller;

	@Override
	public AnswerInformation solve(BigDecimal xLast, BigDecimal xCur, BigDecimal eps) {
		if(isHaveSolve(xLast, xCur)){
			throw new IllegalArgumentException(I18N.get("methods.secant.error"));
		}

		BigInteger n = BigInteger.ZERO;

		checkApprox(xLast, xCur);

		while(!isEnd(xLast, xCur, eps)){
			n = n.add(BigInteger.ONE);
			BigDecimal tmp = new BigDecimal(xCur.toString());
			xCur = nextX(xLast, xCur);
			addHistory(n, xLast, tmp, xCur);
			xLast = tmp;
			draw(xCur);
		}

		return new AnswerInformation(n, xCur, f.function(xCur));
	}

	private void checkApprox(BigDecimal a, BigDecimal b) {
		if(f.function(a).multiply(f.functionDivTwo(a)).compareTo(BigDecimal.ZERO) > 0){
			BigDecimal tmp = new BigDecimal(a.toString());
			a = new BigDecimal(b.toString());
			b = tmp;
		}
	}

	private boolean isEnd(@NonNull BigDecimal xLast,
						  @NonNull BigDecimal xCur,
						  @NonNull BigDecimal eps){
		return xCur.add(xLast.negate()).abs().compareTo(eps) <= 0 || f.function(xCur).abs().compareTo(eps) < 0;
	}

	private boolean isHaveSolve(@NonNull BigDecimal a,
								@NonNull BigDecimal b){
		return f.function(a).multiply(f.function(b)).compareTo(BigDecimal.ZERO) > 0;
	}

	private BigDecimal nextX(BigDecimal xLast, BigDecimal xCur){
		BigDecimal fCur = f.function(xCur);
		BigDecimal left = xCur
				.add(xLast.negate())
				.divide(
						fCur.add(f.function(xLast).negate()
						), MathContext.DECIMAL128)
				.multiply(fCur);
		return xCur.add(left.negate());
	}

	private void draw(@NonNull BigDecimal x){
		if(controller != null) {
			controller.getGraphicsController().writeAPoint(x.doubleValue(), f.function(x).doubleValue());
		}
	}

	private void addHistory(@NonNull BigInteger n,
							@NonNull BigDecimal xLast,
							@NonNull BigDecimal xCur,
							@NonNull BigDecimal xNext){
		if(controller != null) {
			TableData td = TableData.newBuilder()
					.setN(n.longValue())
					.setXLast(xLast.toString())
					.setXCur(xCur.toString())
					.setXNext(xNext.toString())
					.setFx(f.function(xNext).toString())
					.setDiff(xNext.add(xCur.negate()).abs().toString())
					.build();
			controller.getConsoleController().addNewPoint(td);
		}
	}

	@Override
	public void setController(MainController controller) {
		this.controller = controller;
	}
}
