package ru.lanolin.compmath.l3.math;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Functions {

	private static Functions function;
	public static Functions getFunction(){
		return (function == null) ? (function = new Functions()) : function;
	}

	private final BigDecimal a = BigDecimal.valueOf(-2.7);
	private final BigDecimal a1 = BigDecimal.valueOf(-8.1);
	private final BigDecimal b = BigDecimal.valueOf(-1.48);
	private final BigDecimal b1 = BigDecimal.valueOf(-2.96);
	private final BigDecimal c = BigDecimal.valueOf(19.23);
	private final BigDecimal d = BigDecimal.valueOf(6.35);

	private final BigDecimal a2 = BigDecimal.valueOf(-16.2);
	private final BigDecimal b2 = BigDecimal.valueOf(-2.96);

	/**
	 * Оригинальная функция
	 * @param x BigDecimal
	 * @return BigDecimal
	 */
	public BigDecimal function(BigDecimal x){
		return a.multiply(x.pow(3))
				.add(b.multiply(x.pow(2)))
				.add(c.multiply(x))
				.add(d);
	}

	/**
	 * Первая производная
	 * @param x BigDecimal
	 * @return BigDecimal
	 */
	public BigDecimal functionDivOne(BigDecimal x){
		return a1.multiply(x.pow(2))
				.add(b1.multiply(x))
				.add(c);
	}

	/**
	 * Вторая производная
	 * @param x BigDecimal
	 * @return BigDecimal
	 */
	public BigDecimal functionDivTwo(BigDecimal x){
		return a2.multiply(x).add(b2);
	}

	/**
	 * Спец функция
	 */
	public BigDecimal functionXi(BigDecimal x, BigDecimal lambda){
		return this.function(x).multiply(lambda).add(x);
	}

	/**
	 * Производная Спец функция
	 */
	public BigDecimal functionXiDivOne(BigDecimal x, BigDecimal lambda){
		return this.functionDivOne(x).multiply(lambda).add(BigDecimal.ONE);
	}

	@Override
	public String toString() {
		return "f(x) = -2.7 x^3 - 1.48 x^2 + 19.23 x + 6.35";
	}
}
