package ru.lanolin.compmath.l3.math.methods;

import lombok.NonNull;
import ru.lanolin.compmath.l3.fx.I18N;
import ru.lanolin.compmath.l3.fx.controller.MainController;
import ru.lanolin.compmath.l3.fx.controller.TableData;
import ru.lanolin.compmath.l3.math.AnswerInformation;
import ru.lanolin.compmath.l3.math.Functions;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

public class MethodHalfDivision implements Method {

	private static Functions f = Functions.getFunction();

	private MainController controller;

	@Override
	public AnswerInformation solve(@NonNull BigDecimal a,
								   @NonNull BigDecimal b,
								   @NonNull BigDecimal eps) {
		if(isHaveSolve(a, b)){
			throw new IllegalArgumentException(I18N.get("methods.halfdivision.error"));
		}

		BigInteger n = BigInteger.ZERO;
		final BigDecimal two = BigDecimal.valueOf(2);
		BigDecimal x = BigDecimal.ZERO;

		while(!isEnd(a, b, x, eps)){
			x = a.add(b).divide(two, MathContext.DECIMAL128);
            n = n.add(BigInteger.ONE);
            addHistory(n, a, b, x);
			if(isHaveSolve(a, x)){ a = x; }
			else{ b = x; }
			draw(x);
		}

		x = a.add(b).divide(two, MathContext.DECIMAL128);
		addHistory(n.add(BigInteger.ONE), a, b, x);

		return new AnswerInformation(n, x, f.function(x));
	}

	private void draw(@NonNull BigDecimal x){
		if(controller != null) {
//			controller.getGraphicsController().writeATwoLine(a.doubleValue(), b.doubleValue());
			controller.getGraphicsController().writeAPoint(x.doubleValue(), f.function(x).doubleValue());
			Thread.yield();
		}
	}

	private void addHistory(@NonNull BigInteger n,
							@NonNull BigDecimal a,
							@NonNull BigDecimal b,
							@NonNull BigDecimal x){
		if(controller != null) {
			TableData td = TableData.newBuilder()
					.setN(n.longValue())
					.setA(a.toString())
					.setB(b.toString())
					.setXCur(x.toString())
					.setFa(f.function(a).toString())
					.setFb(f.function(b).toString())
					.setFx(f.function(x).toString())
					.setDiff(a.add(b.negate()).abs().toString())
					.build();
			controller.getConsoleController().addNewPoint(td);
		}
	}

	private boolean isHaveSolve(@NonNull BigDecimal a,
								@NonNull BigDecimal b){
		return f.function(a).multiply(f.function(b)).compareTo(BigDecimal.ZERO) > 0;
	}

	private boolean isEnd(@NonNull BigDecimal a,
						  @NonNull BigDecimal b,
						  @NonNull BigDecimal x,
						  @NonNull BigDecimal eps){
		return b.add(a.negate()).abs().compareTo(eps) <= 0 || f.function(x).abs().compareTo(eps) < 0;
	}

	@Override
	public void setController(MainController controller) {
		this.controller = controller;
	}
}
