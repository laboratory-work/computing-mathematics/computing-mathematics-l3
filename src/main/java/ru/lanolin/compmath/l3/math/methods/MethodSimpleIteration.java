package ru.lanolin.compmath.l3.math.methods;

import lombok.NonNull;
import ru.lanolin.compmath.l3.fx.I18N;
import ru.lanolin.compmath.l3.fx.controller.MainController;
import ru.lanolin.compmath.l3.fx.controller.TableData;
import ru.lanolin.compmath.l3.math.AnswerInformation;
import ru.lanolin.compmath.l3.math.Functions;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

public class MethodSimpleIteration implements Method {

	private static Functions f = Functions.getFunction();

	private MainController controller;

	private BigDecimal lambda;

	@Override
	public AnswerInformation solve(BigDecimal a, BigDecimal b, BigDecimal eps) {
		// b = null
		lambda = BigDecimal.ONE.negate().divide(f.functionDivOne(a), MathContext.DECIMAL128);

		if(f.functionXiDivOne(a, lambda).abs().compareTo(BigDecimal.ONE) >= 0){
			throw new IllegalArgumentException(I18N.get("methods.simpleiteration.error"));
		}

		BigInteger n = BigInteger.ZERO;
		BigDecimal xNext;
		BigDecimal xCur = a;
		do {
			n = n.add(BigInteger.ONE);
			draw(xCur);
			xNext = nextX(xCur);
			addHistory(n, xCur, xNext);
			BigDecimal tmp = new BigDecimal(xCur.toString());
			xCur = new BigDecimal(xNext.toString());
			xNext = new BigDecimal(tmp.toString());
		}while (!isEnd(xCur, xNext, eps));

		return new AnswerInformation(n, xCur, f.function(xCur));
	}

	private boolean isEnd(@NonNull BigDecimal xLast,
						  @NonNull BigDecimal xCur,
						  @NonNull BigDecimal eps){
		return xCur.add(xLast.negate()).abs().compareTo(eps) <= 0;
	}

	private BigDecimal nextX(BigDecimal xCur){
		return f.functionXi(xCur, lambda);
	}

	private void draw(@NonNull BigDecimal x){
		if(controller != null) {
			controller.getGraphicsController().writeAPoint(x.doubleValue(), f.function(x).doubleValue());
		}
	}

	private void addHistory(@NonNull BigInteger n,
							@NonNull BigDecimal xCur,
							@NonNull BigDecimal xNext){
		if(controller != null) {
			TableData td = TableData.newBuilder()
					.setN(n.longValue())
					.setXCur(xCur.toString())
					.setXNext(xNext.toString())
					.setFx(f.function(xCur).toString())
					.setDiff(xNext.add(xCur.negate()).abs().toString())
					.build();
			controller.getConsoleController().addNewPoint(td);
		}
	}

	@Override
	public void setController(MainController controller) {
		this.controller = controller;
	}
}
