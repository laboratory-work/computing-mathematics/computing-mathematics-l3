package ru.lanolin.compmath.l3.math;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AnswerInformation {

	private BigInteger n;
	private BigDecimal x;
	private BigDecimal fx;

	@Override
	public String toString() {
		return String.format("N = %d\nX = %e\nf(x) = %e", n, x, fx);
	}
}
