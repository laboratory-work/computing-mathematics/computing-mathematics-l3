package ru.lanolin.compmath.l3;

import javafx.application.Application;
import ru.lanolin.compmath.l3.fx.FXMain;

public class Main {

	public static void main(String[] args) {
		Thread.currentThread().setName("[MAIN]");
		Application.launch(FXMain.class, args);
	}

}
