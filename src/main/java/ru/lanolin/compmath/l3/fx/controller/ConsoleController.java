package ru.lanolin.compmath.l3.fx.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import ru.lanolin.compmath.l3.math.methods.Methods;

public class ConsoleController extends AbstractController {

	@FXML
	private TableView<TableData> table;

	private TableColumn<TableData, Integer> nColumn;
	private TableColumn<TableData, String> xCurColumn;
	private TableColumn<TableData, String> diffColumn;
	private TableColumn<TableData, String> fxColumn;
	private TableColumn<TableData, String> aColumn;
	private TableColumn<TableData, String> bColumn;
	private TableColumn<TableData, String> faColumn;
	private TableColumn<TableData, String> fbColumn;
	private TableColumn<TableData, String> xLastColumn;
	private TableColumn<TableData, String> xNextColumn;
//	private TableColumn<TableData, Integer> fXNextColumn;

	ObservableList<TableColumn<TableData, ?>> methodHalfDivision;
	ObservableList<TableColumn<TableData, ?>> methodSecant;
	ObservableList<TableColumn<TableData, ?>> methodSimpleIteration;

	ObservableList<TableData> items;

	private int index;

	@FXML
	public void initialize() {
		items = FXCollections.observableArrayList();

		nColumn = new TableColumn<>("N");
		nColumn.setCellValueFactory(new PropertyValueFactory<>("N"));
		nColumn.setSortable(true);

		xCurColumn = new TableColumn<>();
		xCurColumn.setCellValueFactory(new PropertyValueFactory<>("xCur"));
		xCurColumn.setSortable(false);

		diffColumn = new TableColumn<>();
		diffColumn.setCellValueFactory(new PropertyValueFactory<>("diff"));
		diffColumn.setSortable(false);

		fxColumn = new TableColumn<>();
		fxColumn.setCellValueFactory(new PropertyValueFactory<>("fx"));
		fxColumn.setSortable(false);

		aColumn = new TableColumn<>("a");
		aColumn.setCellValueFactory(new PropertyValueFactory<>("a"));
		aColumn.setSortable(false);

		bColumn = new TableColumn<>("b");
		bColumn.setCellValueFactory(new PropertyValueFactory<>("b"));
		bColumn.setSortable(false);

		faColumn = new TableColumn<>("f(a)");
		faColumn.setCellValueFactory(new PropertyValueFactory<>("fa"));
		faColumn.setSortable(false);

		fbColumn = new TableColumn<>("f(b)");
		fbColumn.setCellValueFactory(new PropertyValueFactory<>("fb"));
		fbColumn.setSortable(false);

		xLastColumn = new TableColumn<>("x_(i-1)");
		xLastColumn.setCellValueFactory(new PropertyValueFactory<>("xLast"));
		xLastColumn.setSortable(false);

		xNextColumn = new TableColumn<>("x_(i+1)");
		xNextColumn.setCellValueFactory(new PropertyValueFactory<>("xNext"));
		xNextColumn.setSortable(false);

//		fXNextColumn = new TableColumn<>();
//		fXNextColumn.setCellValueFactory(new PropertyValueFactory<>("fXNext"));

		methodHalfDivision = FXCollections.observableArrayList(nColumn, aColumn, bColumn, xCurColumn, faColumn, fbColumn, fxColumn, diffColumn);
		methodSecant = FXCollections.observableArrayList(nColumn, xLastColumn, xCurColumn, xNextColumn, fxColumn, diffColumn);
		methodSimpleIteration = FXCollections.observableArrayList(nColumn, xCurColumn, fxColumn, xNextColumn, diffColumn);

		updateTableColumns(Methods.METHOD_HALF_DIVISION);
		table.setItems(items);
	}

	public void addNewPoint(TableData td){
		items.add(td);
	}

	public void clearTable(){
		items.clear();
		table.getItems().clear();
	}

	public void updateTableColumns(Methods method){
		clearTable();
		switch (method) {
			case METHOD_HALF_DIVISION:
				xCurColumn.setText("x");
				fxColumn.setText("f(x)");
				diffColumn.setText("|a - b|");
				table.getColumns().setAll(methodHalfDivision);
				index = 1;
				break;
			case METHOD_SECANT:
				xCurColumn.setText("x_i");
				fxColumn.setText("f(x_(i+1))");
				diffColumn.setText("|x_(i+1) - x_i|");
//				fXNextColumn.setText("");
				table.getColumns().setAll(methodSecant);
				index = 2;
				break;
			case METHOD_SIMPLE_ITERATION:
				xCurColumn.setText("x_i");
				fxColumn.setText("f(x_i)");
				diffColumn.setText("|x_(i+1) - x_i|");
				table.getColumns().setAll(methodSimpleIteration);
				index = 3;
				break;
		}
	}

	public String getHistory() {
		StringBuilder sb = new StringBuilder();

		for(TableColumn<TableData, ?> tb: table.getColumns()){
			sb.append(tb.getText()).append(" | ");
		}
		sb.append("\n");

		for(TableData td : items) {
			sb.append(td.getN());
			switch (index){
				case 1:
					sb.append(td.getA()).append(" | ")
							.append(td.getB()).append(" | ")
							.append(td.getXCur()).append(" | ")
							.append(td.getFa()).append(" | ")
							.append(td.getFb()).append(" | ")
							.append(td.getFx()).append(" | ");
					break;
				case 2:
					sb.append(td.getXLast()).append(" | ")
							.append(td.getXCur()).append(" | ")
							.append(td.getXNext()).append(" | ")
							.append(td.getFx()).append(" | ");
					break;
				case 3:
					sb.append(td.getXCur()).append(" | ")
							.append(td.getFx()).append(" | ")
							.append(td.getXNext()).append(" | ");
					break;
			}
			sb.append(td.getDiff())
					.append(" |")
					.append("\n");
		}

		return sb.toString();
	}
}
