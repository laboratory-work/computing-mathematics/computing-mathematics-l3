package ru.lanolin.compmath.l3.fx.controller;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.NoArgsConstructor;

public class TableData {

	private SimpleLongProperty N;		// Общее
	private SimpleStringProperty xCur;		// Общее
	private SimpleStringProperty diff;		// Общее
	private SimpleStringProperty fx; 		// Общее

	private SimpleStringProperty a; 		// Метод половинного деления
	private SimpleStringProperty b; 		// Метод половинного деления
	private SimpleStringProperty fa; 		// Метод половинного деления
	private SimpleStringProperty fb; 		// Метод половинного деления

	private SimpleStringProperty xLast;		// Метод секущей
	private SimpleStringProperty xNext;		// Метод секущей / Метод простой итерации
//	private SimpleStringProperty fXNext;	// Метод секущей

	public TableData() {
		N = new SimpleLongProperty();
		xCur = new SimpleStringProperty();
		diff = new SimpleStringProperty();
		fx = new SimpleStringProperty();
		a = new SimpleStringProperty();
		b = new SimpleStringProperty();
		fa = new SimpleStringProperty();
		fb = new SimpleStringProperty();
		xLast = new SimpleStringProperty();
		xNext = new SimpleStringProperty();
	}

	public long getN() {
		return N.get();
	}

	public String getXCur() {
		return xCur.get();
	}

	public String getDiff() {
		return diff.get();
	}

	public String getFx() {
		return fx.get();
	}

	public String getA() {
		return a.get();
	}

	public String getB() {
		return b.get();
	}

	public String getFa() {
		return fa.get();
	}

	public String getFb() {
		return fb.get();
	}

	public String getXLast() {
		return xLast.get();
	}

	public String getXNext() {
		return xNext.get();
	}

//	public String getFXNext() {
//		return fXNext.get();
//	}

	public static Builder newBuilder() {
		return new TableData().new Builder();
	}

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public class Builder {

		public Builder setN(long n) {
			TableData.this.N.set(n);
			return this;
		}

		public Builder setXCur(String xCur) {
			TableData.this.xCur.set(xCur);
			return this;
		}

		public Builder setDiff(String diff) {
			TableData.this.diff.set(diff);
			return this;
		}

		public Builder setFx(String fx) {
			TableData.this.fx.set(fx);
			return this;
		}

		public Builder setA(String a) {
			TableData.this.a.set(a);
			return this;
		}

		public Builder setB(String b) {
			TableData.this.b.set(b);
			return this;
		}

		public Builder setFa(String fa) {
			TableData.this.fa.set(fa);
			return this;
		}

		public Builder setFb(String fb) {
			TableData.this.fb.set(fb);
			return this;
		}

		public Builder setXLast(String xLast) {
			TableData.this.xLast.set(xLast);
			return this;
		}

		public Builder setXNext(String xNext) {
			TableData.this.xNext.set(xNext);
			return this;
		}

		public TableData build() {
			return TableData.this;
		}
	}

//	public void setFXNext(String fXNext) {
//		this.fXNext.set(fXNext);
//	}
}
