package ru.lanolin.compmath.l3.fx;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import javafx.application.Application;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import ru.lanolin.compmath.l3.Main;
import ru.lanolin.compmath.l3.fx.controller.MainController;
import ru.lanolin.compmath.l3.math.methods.Methods;

import java.io.*;
import java.util.Locale;
import java.util.NoSuchElementException;

public class FXMain extends Application {

	private Stage primaryStage;

	private Scene mainScene;

	private MainController mainController;

	@Override
	public void init() throws Exception {
		super.init();
		I18N.setLocale(new Locale("ru", "RU"));

		BorderPane borderPane = new BorderPane();
		FXMLLoader loader = new FXMLLoader(Main.class.getClassLoader().getResources("views/Main.fxml").nextElement());

		AnchorPane mainPane = loader.load();
		this.mainController = loader.getController();

		borderPane.setCenter(mainPane);
		borderPane.setTop(initMenu());

		this.mainScene = new Scene(borderPane);
	}

	private MenuBar initMenu(){
		Menu file = new Menu();
		file.textProperty().bind(I18N.createStringBinding("menu.file.title"));
		MenuItem save = new MenuItem();
		save.textProperty().bind(I18N.createStringBinding("menu.file.save.title"));
		MenuItem upload = new MenuItem();
		upload.textProperty().bind(I18N.createStringBinding("menu.file.open.title"));
		MenuItem exit = new MenuItem();
		exit.textProperty().bind(I18N.createStringBinding("menu.file.exit.title"));

		file.getItems().addAll(
				save,
				upload,
				new SeparatorMenuItem(),
				exit
		);

		exit.setOnAction(this::closeStage);
		save.setOnAction(this::saveActionsMenuItem);
		upload.setOnAction(this::uploadActionsMenuItem);

		Menu setting = new Menu();
		setting.textProperty().bind(I18N.createStringBinding("menu.settings.title"));
		setting.getItems().addAll(
//				getMenuFormatDownloadFile(),
//				getMenuFormatUploadFile(),
				getLanguageMenu()
		);

		Menu help = new Menu();
		help.textProperty().bind(I18N.createStringBinding("menu.help.title"));
		MenuItem about = new MenuItem();
		about.textProperty().bind(I18N.createStringBinding("menu.help.about.title"));
		about.setOnAction(this::openAboutActionsMenuItem);

		help.getItems().addAll(about);

		return new MenuBar(file, setting, help);
	}

	private Menu getLanguageMenu(){
		Menu language = new Menu("Language");
		ToggleGroup langGroup = new ToggleGroup();

		RadioMenuItem en = new RadioMenuItem("English");
		en.setToggleGroup(langGroup);
		en.setOnAction(event -> I18N.setLocale(new Locale("en", "US")));

		RadioMenuItem ru = new RadioMenuItem("Русский");
		ru.setToggleGroup(langGroup);
		ru.setSelected(true);
		ru.setOnAction(event -> I18N.setLocale(new Locale("ru", "RU")));

		language.getItems().addAll(en, ru);
		return language;
	}

//	private Menu getMenuFormatDownloadFile(){
//		Menu formatDownloadFiles = new Menu();
//		formatDownloadFiles.textProperty().bind(I18N.createStringBinding("menu.settings.download.title"));
//		ToggleGroup tgDownload = new ToggleGroup();
//
//		RadioMenuItem downloadFYML = new RadioMenuItem("YAML");
//		downloadFYML.setToggleGroup(tgDownload);
//
//		RadioMenuItem downloadJSON = new RadioMenuItem("JSON");
//		downloadJSON.setToggleGroup(tgDownload);
//		downloadJSON.setSelected(true);
//
//		RadioMenuItem downloadPlain = new RadioMenuItem("Plain");
//		downloadPlain.setToggleGroup(tgDownload);
//
//		formatDownloadFiles.getItems().addAll(downloadFYML, downloadJSON, downloadPlain);
//		return formatDownloadFiles;
//	}
//
//	private Menu getMenuFormatUploadFile(){
//		Menu formatUploadFile = new Menu();
//		formatUploadFile.textProperty().bind(I18N.createStringBinding("menu.settings.upload.title"));
//		ToggleGroup toggleGroupUpload = new ToggleGroup();
//
//		RadioMenuItem uploadYML = new RadioMenuItem("YAML");
//		uploadYML.setToggleGroup(toggleGroupUpload);
//
//		RadioMenuItem uploadJSON = new RadioMenuItem("JSON");
//		uploadJSON.setToggleGroup(toggleGroupUpload);
//		uploadJSON.setSelected(true);
//
//		RadioMenuItem uploadPlain = new RadioMenuItem("Plain");
//		uploadPlain.setToggleGroup(toggleGroupUpload);
//
//		formatUploadFile.getItems().addAll(uploadYML, uploadJSON, uploadPlain);
//		return formatUploadFile;
//	}

	private void openAboutActionsMenuItem(Event event){
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.titleProperty().bind(I18N.createStringBinding("window.about.title"));
		alert.setHeaderText(null);

		TextArea area = new TextArea();
		area.setEditable(false);
		area.setWrapText(true);
		area.textProperty().bind(I18N.createStringBinding("window.about.body"));

		Label label = new Label();
		label.textProperty().bind(I18N.createStringBinding("window.about.title"));

		area.setMaxWidth(Double.MAX_VALUE);
		area.setMaxHeight(Double.MAX_VALUE);
		GridPane.setVgrow(area, Priority.ALWAYS);
		GridPane.setHgrow(area, Priority.ALWAYS);

		GridPane expContent = new GridPane();
		expContent.setMaxWidth(Double.MAX_VALUE);
		expContent.add(label, 0, 0);
		expContent.add(area, 0, 1);

		alert.getDialogPane().setContent(expContent);

		alert.show();
	}

	private FileChooser configureFileChooser() {
		FileChooser fileChooser = new FileChooser();

		FileChooser.ExtensionFilter plain = new FileChooser.ExtensionFilter("Plain (*.txt)", "*.txt");
		FileChooser.ExtensionFilter yml = new FileChooser.ExtensionFilter("YAML (*.yml)", "*.yml");
		FileChooser.ExtensionFilter json = new FileChooser.ExtensionFilter("JSON (*.json)", "*.json");

		fileChooser.getExtensionFilters().addAll(plain, yml, json);
		return fileChooser;
	}

	private void saveActionsMenuItem(Event event){
		FileChooser fc = configureFileChooser();
		File f = fc.showSaveDialog(primaryStage);
		saveToFile(f);
	}

	private void uploadActionsMenuItem(Event event){
		FileChooser fc = configureFileChooser();
		File f = fc.showOpenDialog(primaryStage);
		openFile(f);
	}

	@NoArgsConstructor
	@Data
	private static class P implements Serializable{
		String method;
		String accuracy;
		String a;
		String b;
		String history;
	}

	private void openFile(File f) {
		String fName = f.getName();
		String ext = fName.substring(fName.indexOf('.')+1);

		P data = new P();

		switch (ext){
			case "txt":
			case "yml":
				ObjectMapper yml = new ObjectMapper(new YAMLFactory());
				try {
					data = yml.readValue(f, P.class);
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case "json":
				ObjectMapper json = new ObjectMapper(new JsonFactory());

				try {
					data = json.readValue(f, P.class);
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			default:
				throw new TypeNotPresentException(ext, new NoSuchElementException("Не допустимое расширение"));
		}

		this.mainController.setMethod(Methods.valueOf(data.method));
		this.mainController.setAccuracy(data.accuracy);
		this.mainController.setA(data.a);
		this.mainController.setB(data.b);
	}

	@SneakyThrows
	private void saveToFile(File f) {
		String fName = f.getName();
		String ext = fName.substring(fName.indexOf('.')+1);

		P data = new P();

		data.setMethod(this.mainController.getMethods().name());
		data.setAccuracy(this.mainController.getAccuracy().getText());
		data.setA(this.mainController.getA().getText());
		data.setB(this.mainController.getB().getText());
		data.setHistory(this.mainController.getConsoleController().getHistory());

		String out;
		switch (ext){
			case "txt":
			case "yml":
				ObjectMapper yml = new ObjectMapper(new YAMLFactory());
				out = yml.writeValueAsString(data);
				break;
			case "json":
				ObjectMapper json = new ObjectMapper(new JsonFactory());
				out = json.writeValueAsString(data);
				break;
			default:
				throw new TypeNotPresentException(ext, new NoSuchElementException("Не допустимое расширение"));
		}

		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(f));
			bw.write(out);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
			//Alert write a file
		}
	}

	@Override
	public void start(Stage stage) {
		this.primaryStage = stage;
		this.primaryStage.titleProperty().bind(I18N.createStringBinding("window.title"));

		this.primaryStage.setResizable(false);
		this.primaryStage.setScene(this.mainScene);
		this.primaryStage.setOnCloseRequest(this::closeStage);
		this.primaryStage.show();
	}

	@Override
	public void stop() throws Exception {
		super.stop();
	}

	private void closeStage(Event event){
		System.exit(0);
	}
}
