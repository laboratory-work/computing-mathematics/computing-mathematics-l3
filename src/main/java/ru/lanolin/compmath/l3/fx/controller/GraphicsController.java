package ru.lanolin.compmath.l3.fx.controller;

import javafx.animation.AnimationTimer;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;
import ru.lanolin.compmath.l3.fx.shapes.Axes;
import ru.lanolin.compmath.l3.fx.shapes.Plot;
import ru.lanolin.compmath.l3.fx.shapes.Point;
import ru.lanolin.compmath.l3.math.Functions;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;

public class GraphicsController extends AbstractController{

	@FXML
	private Label function;

	@FXML
	private StackPane pane;

	@FXML
	private Canvas canvas;

	@FXML
	public void initialize() {
		Functions f = Functions.getFunction();
		drawLabel(f);
		initPointsAndGraphics(f);
	}

	Point p;

	AnimationTimer timer;

	private void initPointsAndGraphics(Functions f){
		Axes axes = new Axes(
				400, 230,
				-5,5,0.5,
				-6,6,0.5
		);

		Plot plot = new Plot(
				x -> f.function(BigDecimal.valueOf(x)).doubleValue(),
				-5,5,0.001,
				axes
		);

		pane.getChildren().addAll(plot);
//		pane.setPadding(new Insets(50));
//		pane.setStyle("-fx-background-color: rgb(200, 200, 200);");

		p = new Point(canvas, javafx.scene.paint.Color.BLUE, axes, 0d,0d);
		timer = new AnimationTimer() {
			@Override
			public void handle(long now) {
				p.draw();
			}
		};
		timer.start();
	}

	public void clears(){
		p.clear();
	}

	public void writeAPoint(double x, double y){
		p.setCordPoint(x, y);
	}

	private void drawLabel(Functions f){
		TeXFormula teXFormula = new TeXFormula(f.toString());
		TeXIcon icon = teXFormula.new TeXIconBuilder()
				.setStyle(TeXConstants.STYLE_DISPLAY)
				.setSize(18)
				.build();

		icon.setInsets(new java.awt.Insets(5, 5, 5, 5));
		BufferedImage bi = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);

		JLabel jl = new JLabel();
		jl.setForeground(new Color(0, 0, 0));
		icon.paintIcon(jl, bi.createGraphics(), 0, 0);

		ImageView view = new ImageView(SwingFXUtils.toFXImage(bi, null));
		function.setGraphic(view);
	}

}
