package ru.lanolin.compmath.l3.fx.controller;

import javafx.beans.Observable;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import lombok.Getter;
import ru.lanolin.compmath.l3.fx.I18N;
import ru.lanolin.compmath.l3.math.AnswerInformation;
import ru.lanolin.compmath.l3.math.methods.Method;
import ru.lanolin.compmath.l3.math.methods.Methods;

import java.math.BigDecimal;

public class MainController extends AbstractController{

	private static ObservableList<Methods> methods = FXCollections.observableArrayList(Methods.values());

	@FXML @Getter
	public ConsoleController consoleController;

	@FXML @Getter
	private GraphicsController graphicsController;

	@FXML private ChoiceBox<Methods> choiceBox;
	@FXML private Label labelAccuracy;
	@FXML private Label labelA;
	@FXML private Label labelB;

	@FXML @Getter
	private TextField accuracy;

	@FXML @Getter
	private TextField a;

	@FXML @Getter
	private TextField b;

	@FXML private Button solve;
	@FXML public Button clear;

	@FXML
	public void initialize() {
		choiceBox.setItems(methods);
		choiceBox.getSelectionModel().selectedItemProperty().addListener(this::choiceVariant);

		choiceBox.setValue(Methods.METHOD_HALF_DIVISION);

		labelAccuracy.textProperty().bind(I18N.createStringBinding("controllers.main.labels.accuracy"));
		labelA.textProperty().bind(I18N.createStringBinding("controllers.main.labels.a.first"));
		labelB.textProperty().bind(I18N.createStringBinding("controllers.main.labels.b"));
		solve.textProperty().bind(I18N.createStringBinding("controllers.main.labels.button.solve"));
		clear.textProperty().bind(I18N.createStringBinding("controllers.main.labels.button.clear"));
	}

	private void clearFields(){
		accuracy.setText("");
		a.setText("");
		b.setText("");
	}

	public void setMethod(Methods method){
		choiceBox.getSelectionModel().select(method);
	}

	public void setAccuracy(String accuracy){
		this.accuracy.setText(accuracy);
	}

	public void setA(String a){
		this.a.setText(a);
	}

	public void setB(String b){
		this.b.setText(b);
	}

	private void setVisibleB(boolean visible){
		b.setVisible(visible);
		b.setEditable(visible);
		labelB.setVisible(visible);
	}

	private void choiceVariant(Observable observable, Methods oldV, Methods newV) {
		clearFields();
		switch (newV){
			case METHOD_SIMPLE_ITERATION:
				labelA.textProperty().bind(I18N.createStringBinding("controllers.main.labels.a.second"));
				setVisibleB(false);
				break;
			case METHOD_HALF_DIVISION:
			case METHOD_SECANT:
				labelA.textProperty().bind(I18N.createStringBinding("controllers.main.labels.a.first"));
				labelB.textProperty().bind(I18N.createStringBinding("controllers.main.labels.b"));
				setVisibleB(true);
				break;
		}
		consoleController.updateTableColumns(newV);
	}

	private void setEditable(boolean edit){
		a.setEditable(edit);
		b.setEditable(edit);
		accuracy.setEditable(edit);
		solve.setDisable(!edit);
	}

	private void showAlert(Alert.AlertType type,
						   ObservableValue<? extends String> title,
						   ObservableValue<? extends String> header,
						   String body){
		Alert alert = new Alert(type);
		alert.titleProperty().bind(title);
		alert.headerTextProperty().bind(header);
		alert.setContentText(body);
		alert.show();
	}

	@FXML
	private void buttonPress(){
		clearGraph();
		Methods value = getMethods();
		Method method = value.getMethod();
		method.setController(this);
		setEditable(false);
		AnswerInformation info;
		try {
			checkAllConfirm(value);
			BigDecimal a = new BigDecimal(this.a.getText());
			String bText = this.b.getText();
			BigDecimal b = new BigDecimal(bText.isBlank() || bText.isEmpty() ? "0" : bText);
			info = method.solve(a, b, new BigDecimal(accuracy.getText()));
		} catch (IllegalArgumentException iae) {
			showAlert(Alert.AlertType.ERROR,
					I18N.createStringBinding("controllers.main.alert.error.title"),
					I18N.createStringBinding("controllers.main.alert.error.header.first"),
					iae.getLocalizedMessage());
			return;
		} catch (ArrayStoreException are) {
			showAlert(Alert.AlertType.ERROR,
					I18N.createStringBinding("controllers.main.alert.error.title"),
					I18N.createStringBinding("controllers.main.alert.error.header.second"),
					are.getLocalizedMessage());
			return;
		} finally {
			setEditable(true);
		}
		showAlert(Alert.AlertType.INFORMATION,
				I18N.createStringBinding("controllers.main.alert.info.title"),
				I18N.createStringBinding("controllers.main.alert.info.header"),
				info.toString());
	}

	public Methods getMethods() {
		return choiceBox.getValue();
	}

	private void checkAllConfirm(Methods method) {
		String accuracyT = accuracy.getText();
		String aT = a.getText();
		String bT = b.getText();

		if(accuracyT.isEmpty() || accuracyT.isBlank()){
			throw new ArrayStoreException(I18N.get("controllers.main.alert.error.body.t1"));
		}
		if(new BigDecimal(accuracyT).compareTo(BigDecimal.ONE) > 0){
			throw new ArrayStoreException(I18N.get("controllers.main.alert.error.body.t2"));
		}

		switch (method){
			case METHOD_HALF_DIVISION:
			case METHOD_SECANT:
				BigDecimal aBD = new BigDecimal(aT);
				BigDecimal bBD = new BigDecimal(bT);
				if(aBD.compareTo(bBD) >= 0) {
					throw new ArrayStoreException(I18N.get("controllers.main.alert.error.body.t3"));
				}
				break;
		}
	}

	@FXML
	public void clearGraph() {
		graphicsController.clears();
		consoleController.clearTable();
	}
}
