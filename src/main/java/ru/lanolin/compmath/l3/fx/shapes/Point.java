package ru.lanolin.compmath.l3.fx.shapes;

import javafx.geometry.Bounds;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class Point {

	private Axes axes;
	private GraphicsContext context;

	private final Double h = 3d;
	private final Double w = 3d;

	private final double HEIGHT;
	private final double WIDTH;

	private Bounds inLocal;
	private Cord[] obj;

	public Point(Canvas canvas,
				 Color color, Axes axes, double x, double y
	){
		this.context = canvas.getGraphicsContext2D();
		this.inLocal = canvas.getBoundsInLocal();
		this.axes = axes;

		this.HEIGHT = inLocal.getHeight();
		this.WIDTH = inLocal.getWidth();

		obj = new Cord[1];

		obj[0] = (new Cord(mapX(x), mapY(y), color)); // 0 -> Point
//		obj[1] = (new Cord(mapX(x), mapY(y), Color.DARKGREEN)); // 0 -> Line 1
//		obj[2] = (new Cord(mapX(x), mapY(y), Color.DARKGREEN)); // 0 -> Line 2
	}

	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	private static class Cord {
		private double x;
		private double y;
		private Color color;
	}

	public void setCordPoint(double x, double y){
		obj[0].setX(mapX(x));
		obj[0].setY(mapY(y));
	}

//	public void setCordLine(double x1, double x2){
//		obj[1].setX(mapX(x1));
//		obj[2].setX(mapX(x2));
//	}

	public void draw(){
		context.clearRect(0, 0, WIDTH, HEIGHT);
//		context.save();

//		context.setLineWidth(1);
		context.setFill(obj[0].getColor());
		context.fillOval(obj[0].getX(), obj[0].getY(), w, h);

//		for (int i = 1; i < 3; i++) {
//			context.beginPath();
//			context.moveTo(obj[i].getX(), 0);
//			context.lineTo(obj[i].getX(), HEIGHT);
//			context.setLineWidth(0.4);
//			context.setStroke(obj[i].getColor());
//			context.stroke();
//		}
//
//		context.setStroke(Color.GOLD);
//		context.setLineWidth(2);
//		context.fillRect(obj.get(1).getX(), 0, Math.abs(obj.get(1).getX()-obj.get(2).getX()), HEIGHT);

//		context.restore();
	}

	public void clear(){
		for (Cord cord : obj) {
			cord.setX(mapX(0));
			cord.setY(mapY(0));
		}
		context.clearRect(0, 0, WIDTH, HEIGHT);
	}

	private double mapX(double x) {
		double tx = axes.getPrefWidth() / 2;
		double sx = axes.getPrefWidth() /
				(axes.getXAxis().getUpperBound() - axes.getXAxis().getLowerBound());
		return x * sx + tx;
	}

	private double mapY(double y) {
		double ty = axes.getPrefHeight() / 2;
		double sy = axes.getPrefHeight() /
				(axes.getYAxis().getUpperBound() - axes.getYAxis().getLowerBound());
		return -y * sy + ty;
	}

}
