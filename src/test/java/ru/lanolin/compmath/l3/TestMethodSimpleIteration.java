package ru.lanolin.compmath.l3;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.lanolin.compmath.l3.math.AnswerInformation;
import ru.lanolin.compmath.l3.math.methods.Methods;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TestMethodSimpleIteration extends Assert {

	@Getter
	@RequiredArgsConstructor
	private static class Data {
		@NonNull private BigDecimal a, b, eps, ansX;
		@Setter private AnswerInformation answerInformation;
	}

	private Methods method;

	private List<Data> tests;

	@Before
	public void before(){
		method = Methods.METHOD_SIMPLE_ITERATION;
		tests = new ArrayList<>(3);

		tests.add(new Data(
				BigDecimal.valueOf(-3d),
				BigDecimal.valueOf(-2d),
				BigDecimal.valueOf(0.001d),
				BigDecimal.valueOf(-2.795d)
		));
		tests.add(new Data(
				BigDecimal.valueOf(-1d),
				BigDecimal.valueOf(0d),
				BigDecimal.valueOf(0.001d),
				BigDecimal.valueOf(-0.327d)
		));
		tests.add(new Data(
				BigDecimal.valueOf(3d),
				BigDecimal.valueOf(3d),
				BigDecimal.valueOf(0.001d),
				BigDecimal.valueOf(2.574d)
		));
	}

	private void solve(Data t){
		AnswerInformation ans = method.getMethod().solve(t.getA(), t.getB(), t.getEps());
		System.out.println(ans);
		BigDecimal a2 = ans.getX().add(t.getAnsX().negate()).abs().add(t.getEps().negate());
		int i = a2.compareTo(t.getEps());
		assertTrue(i < 1);
	}

	@Test
	public void test1(){
		solve(tests.get(0));
	}

	@Test
	public void test2(){
		solve(tests.get(1));
	}

	@Test
	public void test3(){
		solve(tests.get(2));
	}

}
